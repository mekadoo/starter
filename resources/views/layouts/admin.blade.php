<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentelella Alela! | </title>
    @include('partials.admin_assets.style')
</head>

<body class="nav-md footer_fixed">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Madeineuromed!</span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                @include('partials.menu-profile')
                <!-- /menu profile quick info -->

                <br />

                <!-- sidebar menu -->
                @include('partials.sidebar')
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                @include('partials.sidebar-footer')
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        @include('partials.top-nav')
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="col-md-12">
                <div class="page-title">
                    <h3> Dashboard </h3>
                    @include('partials.flash-message')
                    <br>
                </div>
                <div class="title_left">
                    @yield('content')
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        @include('partials.footer')
        <!-- /footer content -->
    </div>
</div>

@include('partials.admin_assets.scripts')
</body>
</html>
