<!-- jQuery -->
<script src="{{ asset('dashboard/vendors/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('dashboard/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
{{-- Axios --}}
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<!-- FastClick -->
<script src="{{ asset('dashboard/vendors/fastclick/lib/fastclick.js') }}"></script>
<!-- NProgress -->
<script src="{{ asset('dashboard/vendors/nprogress/nprogress.js') }}"></script>
<!-- jQuery custom content scroller -->
<script src="{{ asset('dashboard/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}"></script>

<!-- PNotify -->
<script src="{{ asset('dashboard/vendors/pnotify/dist/pnotify.js') }}"></script>
<script src="{{ asset('dashboard/vendors/pnotify/dist/pnotify.buttons.js') }}"></script>
<script src="{{ asset('dashboard/vendors/pnotify/dist/pnotify.nonblock.js') }}"></script>


<!-- morris.js -->
<script src="{{ asset('dashboard/vendors/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('dashboard/vendors/morris.js/morris.min.js') }}"></script>
<!-- bootstrap-progressbar -->
<script src="{{ asset('dashboard/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
<!-- bootstrap-daterangepicker -->
<script src="{{ asset('dashboard/vendors/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('dashboard/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>


<!-- Custom Theme Scripts -->
<script src="{{ asset('dashboard/build/js/custom.js') }}"></script>


@yield('scripts')
