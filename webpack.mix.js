const mix = require('laravel-mix');


mix.js([
      'resources/js/app.js',
      'resources/dashboard/vendors/fastclick/lib/fastclick.js',
      'resources/dashboard/vendors/nprogress/nprogress.js',
      'resources/dashboard/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js',
      'resources/dashboard/build/js/custom.min.js',
      ], 
      'public/js')
   .styles([
         'resources/dashboard/vendors/font-awesome/css/font-awesome.min.css',
         'resources/dashboard/vendors/nprogress/nprogress.css',
         'resources/dashboard/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css',
         'resources/dashboard/build/css/custom.min.css',
      ],
      'public/css/app.css')
   .sass('resources/sass/app.scss', 'public/css');
