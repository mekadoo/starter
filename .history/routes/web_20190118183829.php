<?php

Route::get('/admin',function(){
    return redirect('/admin/dashboard');
});

Route::group(['prefix'=>'admin','middleware'=>['auth','is_admin']],function(){
    Route::get('dashboard','Admin\AdminController@index');
    $this->get('logout', 'Auth\LoginController@logout')->name('logout');
});

Route::get('/', 'Auth\LoginController@showLoginForm')->name('home');

Route::auth();
