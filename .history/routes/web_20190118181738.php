<?php

Route::group(['prefix'=>'admin','middleware'=>['auth']],function(){
    $this->get('admin/logout', 'Auth\LoginController@logout')->name('logout');
});

Route::get('/home', 'HomeController@index')->name('home');
