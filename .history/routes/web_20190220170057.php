<?php

// make /admin redirect to dashboard
Route::get('/admin',function(){
    return redirect('/admin/dashboard');
});

// admins routes
Route::group(['prefix'=>'admin','middleware'=>['auth','is_admin']],function(){
    Route::get('dashboard','Admin\AdminController@index');
    $this->get('logout', 'Auth\LoginController@logout')->name('logout');
});

// override default base and make it redirect to login
Route::get('/', 'Auth\LoginController@showLoginForm')->name('home');

Route::auth();
