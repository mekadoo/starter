<?php

Route::group(['prefix'=>'admin','middleware'=>['auth','is_admin']],function(){
    Route::get('dashboard','Admin\AdminController@index');
    $this->get('admin/logout', 'Auth\LoginController@logout')->name('logout');
});

Route::get('/home', 'HomeController@index')->name('home');
