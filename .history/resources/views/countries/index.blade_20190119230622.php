@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_content">
                <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                    <ul class="pagination pagination-split">
                    @foreach (range('A','Z') as $character)
                        <li><a href="/admin/countries/order/{{$character}}">{{ $character }}</a></li>
                    @endforeach
                    <li><a href="/admin/countries/">All</a></li>
                    </ul>
                </div>

                <div class="clearfix"></div>

                @foreach ($countries as $country)
                    <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                        <div class="well profile_view">
                            <div class="col-sm-12">
                                <div class="left col-xs-7">
                                <h2>{{ $country->name }}</h2>
                                <p><strong>About: </strong> {{ str_limit($country->about,$limit=100,$end='...') }} </p>
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-phone"></i> Code #: {{ $country->code }} </li>
                                <li><i class="fa fa-building"></i> Status: <span class="label {{ ($country->is_suspended === 0) ? 'label-primary' : 'label-default' }}  "> {{ ($country->is_suspended === 0) ? 'active' : 'suspended' }} </span> </li>
                                </ul>
                                </div>
                                <div class="right col-xs-5 text-center">
                                <img src="{{ asset('panel/images/user.png') }}" alt="" class="img-circle img-responsive">
                                </div>
                            </div>
                            <div class="col-xs-12 bottom text-center">
                                <div class="col-xs-12 col-sm-6 emphasis">
                                <p class="ratings">
                                    <a>4.0</a>
                                    <a href="#"><span class="fa fa-star"></span></a>
                                    <a href="#"><span class="fa fa-star"></span></a>
                                    <a href="#"><span class="fa fa-star"></span></a>
                                    <a href="#"><span class="fa fa-star"></span></a>
                                    <a href="#"><span class="fa fa-star-o"></span></a>
                                </p>
                                </div>
                                <div class="col-xs-12 col-sm-6 emphasis">
                                    <a href="/panel/sellers/{{$country->slug}}" class="btn btn-primary btn-xs">
                                        <i class="fa fa-user"> </i> View country
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            {{ $countries->links() }}
            </div>
            </div>
        </div>
    </div>
@endsection
