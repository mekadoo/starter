@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_content">
                <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                    <ul class="pagination pagination-split">
                    @foreach (range('A','Z') as $character)
                        <li><a href="/panel/sellers/order/{{$character}}">{{ $character }}</a></li>
                    @endforeach
                    <li><a href="/panel/sellers/">All</a></li>
                    </ul>
                </div>

                <div class="clearfix"></div>

                @foreach ($vendors as $vendor)
                    <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                        <div class="well profile_view">
                            <div class="col-sm-12">
                                <div class="left col-xs-7">
                                <h2>{{ $vendor->name }}</h2>
                                <p><strong>About: </strong> {{ str_limit($vendor->description,$limit=100,$end='...') }} </p>
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-building"></i> Address: {{ $vendor->address }}  </li>
                                    <li><i class="fa fa-phone"></i> Phone #: {{ $vendor->phone }} </li>
                                </ul>
                                </div>
                                <div class="right col-xs-5 text-center">
                                <img src="{{ asset('panel/images/user.png') }}" alt="" class="img-circle img-responsive">
                                </div>
                            </div>
                            <div class="col-xs-12 bottom text-center">
                                <div class="col-xs-12 col-sm-6 emphasis">
                                <p class="ratings">
                                    <a>4.0</a>
                                    <a href="#"><span class="fa fa-star"></span></a>
                                    <a href="#"><span class="fa fa-star"></span></a>
                                    <a href="#"><span class="fa fa-star"></span></a>
                                    <a href="#"><span class="fa fa-star"></span></a>
                                    <a href="#"><span class="fa fa-star-o"></span></a>
                                </p>
                                </div>
                                <div class="col-xs-12 col-sm-6 emphasis">
                                    <a href="/panel/sellers/{{$vendor->slug}}" class="btn btn-primary btn-xs">
                                        <i class="fa fa-user"> </i> View Vendor
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            {{ $vendors->links() }}
            </div>
            </div>
        </div>
    </div>
@endsection
