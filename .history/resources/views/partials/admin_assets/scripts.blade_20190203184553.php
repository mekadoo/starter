<!-- jQuery -->
<script src="{{ asset('dasboard/vendors/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('dasboard/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
{{-- Axios --}}
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<!-- FastClick -->
<script src="{{ asset('dasboard/vendors/fastclick/lib/fastclick.js') }}"></script>
<!-- NProgress -->
<script src="{{ asset('dasboard/vendors/nprogress/nprogress.js') }}"></script>
<!-- jQuery custom content scroller -->
<script src="{{ asset('dasboard/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}"></script>

<!-- PNotify -->
<script src="{{ asset('dasboard/vendors/pnotify/dist/pnotify.js') }}"></script>
<script src="{{ asset('dasboard/vendors/pnotify/dist/pnotify.buttons.js') }}"></script>
<script src="{{ asset('dasboard/vendors/pnotify/dist/pnotify.nonblock.js') }}"></script>


<!-- morris.js -->
<script src="{{ asset('dasboard/vendors/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('dasboard/vendors/morris.js/morris.min.js') }}"></script>
<!-- bootstrap-progressbar -->
<script src="{{ asset('dasboard/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
<!-- bootstrap-daterangepicker -->
<script src="{{ asset('dasboard/vendors/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('dasboard/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>


<!-- Custom Theme Scripts -->
<script src="{{ asset('dasboard/build/js/custom.js') }}"></script>


@yield('scripts')
