<!-- Bootstrap -->
<link href="{{ asset('dashboard/vendors/bootstrap/dist/css/bootstrap.min.css')  }}" rel="stylesheet">
<!-- Font Awesome -->
<link href="{{ asset('dashboard/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
<!-- NProgress -->
<link href="{{ asset('dashboard/vendors/nprogress/nprogress.css') }}" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="{{ asset('dashboard/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css') }}" rel="stylesheet"/>
<!-- bootstrap-daterangepicker -->
<link href="{{ asset('dashboard/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">

<!-- Custom Theme Style -->
<link href="{{ asset('dashboard/build/css/custom.min.css') }}" rel="stylesheet">
