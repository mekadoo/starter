<!-- jQuery -->
<script src="{{ asset('dashboard/vendors/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('dashboard/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('dashboard/vendors/fastclick/lib/fastclick.js') }}"></script>
<!-- NProgress -->
<script src="{{ asset('dashboard/vendors/nprogress/nprogress.js') }}"></script>
<!-- morris.js -->
<script src="{{ asset('dashboard/vendors/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('dashboard/vendors/morris.js/morris.min.js') }}"></script>
<!-- jQuery custom content scroller -->
<script src="{{ asset('dashboard/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}"></script>

<!-- Custom Theme Scripts -->
<script src="{{ asset('dashboard/build/js/custom.min.js') }}"></script>

@yield('scripts')
