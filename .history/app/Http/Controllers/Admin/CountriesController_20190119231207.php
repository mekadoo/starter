<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use \App\Models\Country;

use Faker\Generator as Faker;

class CountriesController extends Controller
{
    public function index()
    {
        $countries = Country::paginate(20);
       return view('countries.index', compact('countries'));
    }

    public function viewCountry($id)
    {
        $country = self::getCountrById($id);
        return $country;
    }

    private static function getCountrById($id)
    {
        $country = Country::where('id',$id)->first();
        return $country;
    }
}
