<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use \App\Country;

class CountriesController extends Controller
{
    public function index()
    {
        $countries = Country::paginate(20);
        return view('countries.index');
    }
}
