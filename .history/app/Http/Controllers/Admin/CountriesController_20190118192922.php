<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use \App\Models\Country;

class CountriesController extends Controller
{
    public function index()
    {
        $countries = Country::paginate(20);
        return view('countries.index',compact('countries'));
        [         "Albania",
        "+355"=>"Algeria",
        "+213"=>"Austria",
        "+43"=>
             "Belgium",
        "+32"=>
             "Bosnia and Herzegovina",
        "+387"=>
             "Bulgaria",
        "+359"=>
             "Croatia",
        "+385"=>
             "Cyprus",
        "+357"=>
             "Czech Republic",
        "+420"=>
             "Denmark",
        "+45"=>
             "Egypt",
        "+20"=>
             "Estonia",
        "+372"=>
             "Finland",
        "+358"=>
             "France",
        "+33"=>
             "Germany",
        "+49"=>
             "Greece",
        "+30"=>
             "Hungary",
        "+36"=>
             "Iceland",
        "+354"=>
             "Ireland",
        "+353"=>
             "Israel",
        "+972"=>
             "Italy",
        "+39"=>
             "Jordan",
        "+962"=>
             "Latvia",
        "+371"=>
             "Lebanon",
        "+961"=>
             "Liechtenstein",
        "+423"=>
             "Lithuania",
        "+370"=>
             "Luxembourg",
        "+352"=>
             "Malta",
        "+356"=>
             "Mauritania",
        "+222"=>
             "Monaco",
        "+377"=>
             "Montenegro",
        "+382"=>
             "Morocco",
        "+212"=>
             "Netherlands",
        "+31"=>
             "Norway",
        "+47"=>
             "Palestine",
        "+970"=>
             "Poland",
        "+48"=>
             "Portugal",
        "+351"=>
             "Romania",
        "+40"=>
             "Serbia",
        "+381"=>
             "Slovakia",
        "+421"=>
             "Slovenia",
        "+386"=>
             "Spain",
        "+34"=>
             "Sweden",
        "+46"=>
             "Switzerland",
        "+41"=>
             "Syria",
        "+963"=>
             "Tunisia",
        "+216"=>
             "Turkey",
        "+90"=>
             "United Kingdom",
        "+44"]=>
    }
}
