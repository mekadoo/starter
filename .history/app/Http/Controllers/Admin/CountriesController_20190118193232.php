<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use \App\Models\Country;

class CountriesController extends Controller
{
    public function index()
    {
        $countries = Country::paginate(20);
        return view('countries.index', compact('countries'));
        $new = [
            "+355"=>
            "Albania",
            "+213"=>
            "Algeria",
            "+43"=>
            "Austria",
            "+32"=>
            "Belgium",
            "+387"=>
            "Bosnia and Herzegovina",
            "+359"=>
            "Bulgaria",
            "+385"=>
            "Croatia",
            "+357"=>
            "Cyprus",
            "+420"=>
            "Czech Republic",
            "+45"=>
            "Denmark",
            "+20"=>
            "Egypt",
            "+372"=>
            "Estonia",
            "+358"=>
            "Finland",
            "+33"=>
            "France",
            "+49"=>
            "Germany",
            "+30"=>
            "Greece",
            "+36"=>
            "Hungary",
            "+354"=>
            "Iceland",
            "+353"=>
            "Ireland",
            "+972"=>
            "Israel",
            "+39"=>
            "Italy",
            "+962"=>
            "Jordan",
            "+371"=>
            "Latvia",
            "+961"=>
            "Lebanon",
            "+423"=>
            "Liechtenstein",
            "+370"=>
            "Lithuania",
            "+352"=>
            "Luxembourg",
            "+356"=>
            "Malta",
            "+222"=>
            "Mauritania",
            "+377"=>
            "Monaco",
            "+382"=>
            "Montenegro",
            "+212"=>
            "Morocco",
            "+31"=>
            "Netherlands",
            "+47"=>
            "Norway",
            "+970"=>
            "Palestine",
            "+48"=>
            "Poland",
            "+351"=>
            "Portugal",
            "+40"=>
            "Romania",
            "+381"=>
            "Serbia",
            "+421"=>
            "Slovakia",
            "+386"=>
            "Slovenia",
            "+34"=>
            "Spain",
            "+46"=>
            "Sweden",
            "+41"=>
            "Switzerland",
            "+963"=>
            "Syria",
            "+216"=>
            "Tunisia",
            "+90"=>
            "Turkey",
            "+44=>
            "United Kingdom",
        ];
    }
}
